//#region Sneaky credentials.
const authToken = '<redacted>';
const username = '<redacted>'
//#endregion

const options = {
    "identity": {
        "username": username,
        "password": authToken
    },
    "channels": [],
    "port": 80,
    "url": "ws://irc-ws.chat.twitch.tv",
    "subscribe": {  /* Currently Unused. */
        "topics": [''],
        "url": "wss://pubsub-edge.twitch.tv"
    },
    "keywords": [],
    "cmdtoken": "!"
};

export {options}