import { options } from './settings';
const notification = new Audio('./donk.mp3');

const dom = {
    "target": document.getElementById("target"),
    "add": (dataObject) => {
        let child = document.createElement('div');
        prepareChildNode(dataObject['display-name'], dataObject.color, dataObject.message, child);
        if (shouldHighlight(options.keywords, dataObject.message))
            highlight(child);
        setExpire(child, 20);
        target.appendChild(child);
    },
    "parseEmotes": (dataObject) => {
        if (dataObject.emotes.length > 0) {
            let slices = [];
            let motes = dataObject.emotes.split("/");
            for (let emote of motes) {
                emote = emote.split(":");
                let index = emote[1].split("-");
                let begin = Number.parseInt(index[0]);
                let end = Number.parseInt(index[1]) + 1;
                slices.push({
                    "str": dataObject.message.slice(begin, end),
                    "url": '<img class="chat-emote" src="http://static-cdn.jtvnw.net/emoticons/v1/' + emote[0] + '/1.0">'
                });
            }
            for (let replace of slices) {
                dataObject.message = dataObject.message.replace(replace.str, replace.url);
            }
        }
        return dataObject.message;
    }
}

const msgUtil = {
    "parseMessage": (message) => {
        let returnObj = {};
        message = message.split("PRIVMSG");
        returnObj.channel = message[1].match(/#[a-z0-9]+/)[0];
        returnObj.message = htmlEncode(message[1].split(/:(.+)/)[1]);
        for (let item of message[0].split(";")) {
            item = item.split("=");
            returnObj[item[0]] = item[1];
        }
        return returnObj;
    },
    "isCommand": (message) => {
        return message.indexOf(options.cmdtoken) == 0;
    },
    "formatWhisper": (recipient, message) => {
        recipient = recipient.toLowerCase();
        return "PRIVMSG #jtv :/w " + recipient + " " + message;
    },
    "formatChannelResponse": (channel, message) => {
        return "PRIVMSG " + channel + " :" + message;
    }
}

const audio = {
    "notify": () => { notification.play(); }
}

function htmlEncode(message) {
    message = message.replace(/</g, "&#x3C;");
    message = message.replace(/>/g, "&#x3E;");
    return message;
}

function shouldHighlight(keywords, message) {
    for (let keyword of keywords) {
        keyword = keyword.toLowerCase();
        message = message.toLowerCase();
        let match = new RegExp(`\\b${keyword}\\b`);
        if (message.match(match) !== null)
            return true;
    }
    return false;
}

function highlight(child) {
    window.setTimeout(() => {
        child.classList.remove('fadeInUp');
        child.classList.add('bounce', 'highlighted');
    }, 2400);
}

function setExpire(child, delay) {
    window.setTimeout(() => {
        child.classList.remove('fadeInUp');
        child.classList.add('fadeOutUp');
    }, delay * 1000);
}

function prepareChildNode(name, color, message, child) {
    child.innerHTML = `<span class="username-style" style="color: ${color};">${name}</span>: ${message}`;
    child.classList.add('animated', 'fadeInUp', 'message-container');
}

export { audio, dom, msgUtil }