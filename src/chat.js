import { Subject } from 'rxjs';
import { commands } from './commands';
import { options } from './settings';
import { audio, dom, msgUtil } from './utility';

var ws;

// Subjects
var rawStream = new Subject();
var messageStream = new Subject();
var sysStream = new Subject();
var commandStream = new Subject();
var domStream = new Subject();

// Raw socket messages.
rawStream.subscribe(data => {
    if (data.data.indexOf("PRIVMSG") > -1) {
        let dataObject = msgUtil.parseMessage(data.data);
        messageStream.next(dataObject);
    }
    else
        sysStream.next(data.data);
});

// PRIVMSG:
messageStream.subscribe(data => {
    if (msgUtil.isCommand(data.message))
        commandStream.next(data);
    domStream.next(data);
});

// Everything not PRIVMSG
sysStream.subscribe(message => {
    if (message.indexOf("PING") == 0) {
        ws.send("PONG :tmi.twitch.tv");
        console.log("responded to ping.");
    }
});

// Dom Manipulation.
domStream.subscribe(data => {
    data.message = dom.parseEmotes(data);
    dom.add(data);
    audio.notify();
});

// stream commands.
commandStream.subscribe(data => {
    let command = parseCommand(data);
    if (!command)
        return;
    command.act();
    if (command.response) {
        if (command.shouldWhisper) {
            ws.send(msgUtil.formatWhisper(data['display-name'], command.response));
        }
        else
            ws.send(msgUtil.formatChannelResponse(data.channel, command.response));
    }
});

function connect() {
    ws = new WebSocket(options.url.concat(":", options.port));
    ws.onopen = () => {
        ws.send("CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership");
        ws.send(`PASS ${options.identity.password}`);
        ws.send(`NICK ${options.identity.username}`);
        for (let chan of options.channels)
            ws.send(`JOIN  #${chan}`);
    };

    ws.onmessage = (data) => {
        rawStream.next(data);
    }
}


function parseCommand(data) {
    let message = data.message.slice(options.cmdtoken.length).trim();
    let tokens = message.split(" ");
    let cmd = tokens[0];
    let params = tokens.slice(1);
    if (commands[cmd]) {
        return commands[cmd](params, data);
    }
}

(() => {
    connect();
})();

//handle emoticons http://static-cdn.jtvnw.net/emoticons/v1/emote-id/3.0 - the last number is the scale.