const path = require('path');

module.exports = {
  entry: './src/chat.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  }
};