# Project Requirements

To run this project you will need to have Node and NPM installed.

You can get these here: https://nodejs.org/en/

The latest LTS version should be sufficient.

## installation instructions

* Install Node

* Navigate to the project directory on Terminal or Command Prompt

* run `npm install`

  * A folder named node_modules should have been created.

* Modify the *settings.js* file to include the username and OAuth Token for your bot account.


*To generate an OAuth token visit this site while logged into your bot account* https://twitchapps.com/tmi/

*this step may become unnecessary in later versions when OAuth will be implemented programatically*



* Add all channels you want to show chat from to the `channels` array, separated by commas. For example: `"channels": ["mychannel", "anotherchannel" ...]`
* Add words to the `keywords` array you want to be highlighted with an extra animation on screen.
* run `npm run build`
  * A folder named dist should have been created containing a file *main.js*

### You're now ready to listen to chat messages on twitch.

Simply open *index.html* with an active internet connection and you're good to go.



**Notice** to add the overlay to OBS simply add a new "Browser Source" and browse to the index.html file.

*The chat is full-screen width so it will automatically wrap to the size parameters set in the source control in OBS.*